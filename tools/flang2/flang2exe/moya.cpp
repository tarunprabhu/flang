#include "moya.h"
#include "dtypeutl.h"
#include "fih.h"
#include "ili.h"
#include "llassem.h"
#include "symfun.h"
#include <execinfo.h>
#include <stdio.h>

#include <dlfcn.h>

#include <algorithm>
#include <deque>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <vector>

// Declaration of Moya functions that will be called when saving frontend
// information there
//
// FIXME: This is a really terrible way of doing things. But all these
// functions have to be assigned with a call to INIT_API_FUNC in moya_init()
// If you forget to do this, it will fail in very strange ways
//
DECL_API_FUNC(moya_const_get_array_index_marker, int64_t (*)(void));
DECL_API_FUNC(moya_module_add_fmodule,
              void (*)(const char*, const char*, unsigned, unsigned));
DECL_API_FUNC(moya_module_add_function,
              void (*)(const char*,
                       const char*,
                       const char*,
                       const char*,
                       unsigned,
                       unsigned,
                       int));
DECL_API_FUNC(moya_module_add_global,
              void (*)(const char*,
                       const char*,
                       const char*,
                       const char*,
                       unsigned,
                       const char* const*,
                       const unsigned*,
                       const char* const*,
                       const unsigned*));
DECL_API_FUNC(moya_module_add_layout_descriptor, void (*)(const char*));
DECL_API_FUNC(moya_module_add_closure, void (*)(const char*, const char*));
DECL_API_FUNC(moya_module_get_min_allocated_type_id, int64_t (*)(const char*));
DECL_API_FUNC(moya_module_get_max_allocated_type_id, int64_t (*)(const char*));
DECL_API_FUNC(moya_module_get_allocated_type, int (*)(const char*, int64_t));
DECL_API_FUNC(moya_module_is_global_category, int (*)(const char*));
DECL_API_FUNC(moya_module_get_global_category_module,
              const char* (*)(const char*));
DECL_API_FUNC(moya_module_add_struct, void (*)(const char*));
DECL_API_FUNC(moya_module_add_typedesc, void (*)(const char*));
DECL_API_FUNC(moya_module_add_local_array_2,
              void (*)(const char*, const char*, const char*));
DECL_API_FUNC(moya_module_get_num_interface_params,
              unsigned (*)(const char*, unsigned));
DECL_API_FUNC(moya_module_get_interface_params,
              const int* (*)(const char*, unsigned));
DECL_API_FUNC(moya_struct_add_type,
              void (*)(const char*, const char*, unsigned, const char*, bool));
DECL_API_FUNC(
    moya_function_add_arg,
    void (*)(const char*, const char*, const char*, int, int, int, int));
DECL_API_FUNC(moya_function_add_child, void (*)(const char*, const char*));
DECL_API_FUNC(moya_closure_add_type,
              void (*)(const char*, const char*, const char*, bool, bool));
DECL_API_FUNC(moya_function_add_allocated_type,
              void (*)(const char*, int64_t, const char*));
DECL_API_FUNC(
    moya_function_add_gep,
    void (*)(const char*, int64_t, int64_t, bool, const int64_t*, uint64_t));

static bool flang_curr_has_internal() {
  return gbl.internal == 1;
}

static bool flang_curr_is_internal() {
  return gbl.internal > 1;
}

static std::string flang_curr_get_file() {
  std::stringstream ss;
  ss << FIH_DIRNAME(1) << "/" << FIH_FILENAME(1);
  return ss.str();
}

static bool is_local(SPTR sptr) {
  return SCG(sptr) == SC_LOCAL or SCG(sptr) == SC_STATIC;
}

static bool is_string(SPTR sptr) {
  return DTY(DTYPEG(sptr)) == TY_CHAR or DTY(DTYPEG(sptr)) == TY_NCHAR;
}

static bool is_array(SPTR sptr) {
  return DTY(DTYPEG(sptr)) == TY_ARRAY;
}

static bool is_array_desc(SPTR sptr) {
  return DESCARRAYG(sptr);
}

namespace {
class Function;
class Module;
} // namespace

static ::Module& module_get();
static ::Function& func_get();
static ::Function& func_get_parent();
static ::Function& func_add();
static void func_remove();
static void func_parent_remove();
static bool func_added();
static bool func_parent_added();

namespace {

template <typename T>
class Component {
private:
  SPTR sptr;
  std::string name;
  std::string type;
  uint64_t offset;
  bool pointer;
  bool descriptor;
  T* parent;

public:
  Component(SPTR sptr,
            const std::string& name,
            const std::string& type,
            long offset,
            bool descriptor,
            T* parent = nullptr)
      : sptr(sptr), name(name), type(type), offset(offset), pointer(false),
        descriptor(descriptor), parent(parent) {
    ;
  }

  Component(SPTR sptr,
            const std::string& name,
            const std::string& type,
            bool pointer,
            bool descriptor,
            T* parent = nullptr)
      : sptr(sptr), name(name), type(type), offset(0), pointer(pointer),
        descriptor(descriptor), parent(parent) {
    ;
  }

  void setParent(T* parent) {
    this->parent = parent;
  }

  SPTR getSptr() const {
    return sptr;
  }

  const std::string& getName() const {
    return name;
  }

  const std::string& getType() const {
    return type;
  }

  long getOffset() const {
    return offset;
  }

  bool getPointer() const {
    return pointer;
  }

  bool getDescriptor() const {
    return descriptor;
  }

  bool hasParent() const {
    return parent;
  }

  const T& getParent() const {
    return *parent;
  }

  bool operator==(const Component& other) const {
    return (sptr == other.sptr) and (name == other.name)
           and (type == other.type) and (offset == other.offset)
           and (pointer == other.pointer) and (descriptor == other.descriptor);
  }

  bool operator!=(const Component& other) const {
    return not(*this == other);
  }
};

//     When can their glory fade?
//     O the wild charge they made!
//     All the world wondered.
//
// This is one of those charming global variables associated with a function
// that contains _____ (those blanks will be filled whenver I can figure out
// what actually goes in there) and is basically a flattened byte array
// in LLVM but has meaningful structure that could potentially be exploited
// by someone insane enough to do so
//
// At least in one case, the module level variables get wrapped into a
// FakeGlobal
//
class FakeGlobal {
public:
  enum Kind {
    ModuleBegin = -1,

    ModulePublicAny = 0,
    ModulePublicChar = 1,
    ModulePublicLong = 2,
    ModulePublicThreadLocal = 3,

    ModulePrivateAny = 4,
    ModulePrivateChar = 5,
    ModulePrivateLong = 6,
    ModulePrivateThreadLocal = 7,

    ModulePublicAnyInit = 8,
    ModulePublicCharInit = 9,
    ModulePublicLongInit = 10,
    ModulePublicThreadLocalInit = 11,

    ModulePrivateAnyInit = 12,
    ModulePrivateCharInit = 13,
    ModulePrivateLongInit = 14,
    ModulePrivateThreadLocalInit = 15,

    ModuleUnknown = 33,

    ModuleEnd = 34,

    FunctionBegin = 64,
    FunctionBSS = 65,
    FunctionStatics = 66,
    FunctionEnd = 72,

    Unknown = 128
  };

private:
  SPTR sptr;
  Kind kind;
  std::string name;
  std::string type;
  std::string parent;
  std::vector<Component<FakeGlobal>> components;

public:
  FakeGlobal(SPTR sptr,
             const std::string& name,
             const std::string& type,
             Kind kind,
             const std::string& parent)
      : sptr(sptr), name(name), type(type), kind(kind), parent(parent) {
    ;
  }

  FakeGlobal(SPTR sptr,
             const std::string& name,
             const std::string& type,
             Kind kind,
             const std::string& parent,
             const std::vector<Component<FakeGlobal>>& components)
      : sptr(sptr), name(name), type(type), kind(kind), parent(parent),
        components(components) {
    for(Component<FakeGlobal>& component : this->components)
      component.setParent(this);
  }

  SPTR getSptr() const {
    return sptr;
  }

  Kind getKind() const {
    return kind;
  }

  const std::string& getName() const {
    return name;
  }

  const std::string& getType() const {
    return type;
  }

  const std::string& getParent() const {
    return parent;
  }

  void addComponent(SPTR sptr,
                    const std::string& name,
                    const std::string& type,
                    long offset,
                    bool descriptor) {
    return components.emplace_back(sptr, name, type, offset, descriptor, this);
  }

  void addComponent(SPTR sptr,
                    const std::string& name,
                    const std::string& type,
                    bool pointer,
                    bool descriptor) {
    return components.emplace_back(sptr, name, type, pointer, descriptor, this);
  }

  const std::vector<Component<FakeGlobal>>& getComponents() const {
    return components;
  }

  const Component<FakeGlobal>& getComponent(unsigned i) const {
    return components.at(i);
  }

  bool isChar() const {
    switch(kind) {
    case Kind::ModulePublicChar:
    case Kind::ModulePrivateChar:
    case Kind::ModulePublicCharInit:
    case Kind::ModulePrivateCharInit:
      return true;
    default:
      return false;
    }
  }

  bool operator==(const FakeGlobal& other) const {
    if((name != other.name) or (type != other.type) or (kind != other.kind)
       or (parent != other.parent))
      return false;
    if(components.size() != other.components.size())
      return false;
    for(unsigned i = 0; i < components.size(); i++)
      if(components.at(i) != other.components.at(i))
        return false;
    return true;
  }

  bool operator!=(const FakeGlobal& other) const {
    return not(*this == other);
  }

public:
  static bool isFunctionGlobal(FakeGlobal::Kind kind) {
    if((kind > Kind::FunctionBegin) and (kind < Kind::FunctionEnd))
      return true;
    return false;
  }

  static bool isModuleGlobal(FakeGlobal::Kind kind) {
    if((kind > Kind::ModuleBegin) and (kind < Kind::ModuleEnd))
      return true;
    return false;
  }

  static bool isPublic(FakeGlobal::Kind kind) {
    switch(kind) {
    case ModulePublicAny:
    case ModulePublicChar:
    case ModulePublicLong:
    case ModulePublicThreadLocal:
    case ModulePublicAnyInit:
    case ModulePublicCharInit:
    case ModulePublicLongInit:
    case ModulePublicThreadLocalInit:
      return true;
    default:
      return false;
    }
  }

  static bool isPrivate(FakeGlobal::Kind kind) {
    switch(kind) {
    case ModulePrivateAny:
    case ModulePrivateChar:
    case ModulePrivateLong:
    case ModulePrivateThreadLocal:
    case ModulePrivateAnyInit:
    case ModulePrivateCharInit:
    case ModulePrivateLongInit:
    case ModulePrivateThreadLocalInit:
      return true;
    default:
      return false;
    }
  }

  static bool isInit(FakeGlobal::Kind kind) {
    switch(kind) {
    case ModulePublicAnyInit:
    case ModulePublicCharInit:
    case ModulePublicLongInit:
    case ModulePublicThreadLocalInit:
    case ModulePrivateAnyInit:
    case ModulePrivateCharInit:
    case ModulePrivateLongInit:
    case ModulePrivateThreadLocalInit:
      return true;
    default:
      return false;
    }
  }

  static bool isThreadLocal(FakeGlobal::Kind kind) {
    switch(kind) {
    case ModulePublicThreadLocal:
    case ModulePrivateThreadLocal:
    case ModulePublicThreadLocalInit:
    case ModulePrivateThreadLocalInit:
      return true;
    default:
      return false;
    }
  }

  static bool is8(FakeGlobal::Kind kind) {
    switch(kind) {
    case ModulePublicChar:
    case ModulePrivateChar:
    case ModulePublicCharInit:
    case ModulePrivateCharInit:
      return true;
    default:
      return false;
    }
  }

  static bool is64(FakeGlobal::Kind kind) {
    switch(kind) {
    case ModulePublicLong:
    case ModulePrivateLong:
    case ModulePublicLongInit:
    case ModulePrivateLongInit:
      return true;
    default:
      return false;
    }
  }
};

// File-level metadata. This corresponds directly to an LLVM modules
class Module {
private:
  std::set<std::string> tds;
  std::set<std::string> layoutDescriptors;

public:
  Module() : tds({"struct.ld.memtype"}) {
    ;
  }

  Module(const Module&) = delete;
  virtual ~Module() = default;

  void addTypeDesc(const std::string& base) {
    std::string s = base;
    if(base[0] == '%')
      s = s.substr(1);
    tds.insert(s.c_str());
  }

  void addLayoutDescriptor(const std::string& name) {
    layoutDescriptors.insert(name);
  }

  void process(const LL_Module& llvmModule) const {
    for(const std::string& s : tds) {
      moya_iprint("type descriptor: %s\n", s.c_str());
      moya_module_add_typedesc(s.c_str());
    }

    for(const std::string& g : layoutDescriptors) {
      moya_iprint("layout descriptor: %s\n", g.c_str());
      moya_module_add_layout_descriptor(g.c_str());
    }

    //     Stormed at with shot and shell,
    //     Boldly they rode and well,
    //     Into the jaws of Death,
    //     Into the mouth of hell
    //     Rode the six hundred.
    //
    // I am firmly convinced that if Flang were to be wiped out of existence,
    // the entropy of our universe will come crashing down - Second Law of
    // Thermodynamics be damned!
    //
    // If a member of a struct in an array is accessed, e.g.
    //
    //   t = struct % objects(i) % field
    //
    // Flang helpfully "reverses" the offset calculation. Instead of first
    // calculating the offset to the appropriate array element and then
    // computing the offset to the field, it first computes the offset of the
    // field and then with the pointer suitably adjusted, moves to the
    // appropriate array element. (I will give Flang the benefit of the doubt
    // and assume that this is deterministic, but note my utter lack of
    // astonishment if this is not the case).
    //
    // So we keep track of all the elements of a struct that are arrays so if
    // we ever see this taking place, we can special case it
    //
    moya_iprint("user structs: %ld\n", llvmModule.num_user_structs);
    for(unsigned i = 0; i < llvmModule.num_user_structs; i++) {
      if(LL_Type* sty = llvmModule.user_structs.values[i]->type_struct) {
        // It seems as if a number of user structs get included in every
        // function even if none of them are used. This ends up with a lot
        // of structs with no bodies. So we just ignore anything that doesn't
        // have any elements.
        //
        // FIXME: The consequence of this is that any actual zero-element
        // structs will be ignored. But I am willing to live with that
        // limitation.
        //
        if(sty->sub_elements) {
          std::string sname = sty->str;
          if(sname.length() and (sname[0] == '%'))
            sname = sname.substr(1);
          moya_iprint("%4d: %s (%d)\n", i, sname.c_str(), sty->sub_elements);
          moya_module_add_struct(sname.c_str());
          for(unsigned i = 0; i < sty->sub_elements; i++) {
            moya_iprint("%8d: %s => %s (%d)\n",
                        i + 1,
                        sty->sub_names[i],
                        sty->sub_types[i]->str,
                        sty->sub_flags[i]);
            moya_struct_add_type(sname.c_str(),
                                 sty->sub_names[i],
                                 sty->sub_offsets[i],
                                 sty->sub_types[i]->str,
                                 sty->sub_flags[i]);
          }
        }
      }
    }
  }
};

class GEP {
public:
  int ilix;
  int op1;
  int op2;
  int64_t offset;
  int64_t cumulative;
  bool array;
  std::vector<int64_t> offsets;

public:
  GEP(int ilix, int op1, int op2)
      : ilix(ilix), op1(op1), op2(op2), offset(0), cumulative(0), array(false) {
    ;
  }

  void addOffset(int64_t offset, bool ignore) {
    if(ignore) {
      // There should never be a negative offset anywhere.
      // We will either be looking at the field of a struct or an element of
      // an array. In the latter case, it should always be zero because we
      // will never look at anything but the first element of the array.
      // But a zero offset is ambiguous because it could still be a zero
      // offset into a struct, so for arrays (or strings), it will be -1.
      // The second reason for this is that when propagating the analysis
      // types for GEP instructions, the pointer argument to a GEP instruction
      // could actually represent an array of that type. In that case, the
      // first element of the offsets should be "array zero". If we just add
      // a regular zero and the pointer is to a struct, we cannot be certain
      // whether we are looking at a field of the struct or not
      //
      this->offsets.push_back(moya_const_get_array_index_marker());
    } else {
      this->offsets.push_back(offset);
      this->offset += offset;
    }
    this->cumulative += offset;
  }

  void sanityCheck() {
    if(array) {
      if(not((offsets.size() == 1)
             and (offsets.at(0) == moya_const_get_array_index_marker()))) {
        std::stringstream ss;
        ss << "[";
        for(int64_t offset : offsets)
          ss << " " << offset;
        ss << "]";
        std::string offs = ss.str();
        moya_error("Inconsistent GEP\n"
                   "    ilix: %d\n"
                   "  offset: %ld\n"
                   "   cumul: %ld\n"
                   "   array: %d\n"
                   "    offs: %s\n",
                   ilix,
                   offset,
                   cumulative,
                   array,
                   offs.c_str());
      }
    }
  }

  void finalize() {
    sanityCheck();

    std::reverse(offsets.begin(), offsets.end());
  }
};

class ILI {
public:
  int ilix;
  int op1, op2;

public:
  ILI(int ilix, int op1, int op2) : ilix(ilix), op1(op1), op2(op2) {
    ;
  }
};

class ILM {
public:
  int ilm;
  ILM_OP opc;
  int depth;

public:
  ILM(int ilm, ILM_OP opc, int depth = -1) : ilm(ilm), opc(opc), depth(depth) {
    ;
  }
  ILM(const ILM&) = default;
};

// Describes an offset that was calculated while generating a GEP instruction
class Offset {
public:
public:
  // This is the key to uniquely identify this Offset
  int ilix;

  // This is only used for final offsets
  SPTR sptr;

  // The actual value that we need for the GEP
  int64_t offset;

  // The previous element in the chain (0 if there is no previous element)
  int prev;

  // Ignore this value when computing the offset. This is needed when looking
  // up elements of arrays or strings
  bool ignore;

  // True if this offset represents an array lookup of a struct and not an
  // offset into the field of a struct
  bool smove;

  // The ILM instruction in which this was first created. It may be used
  // else where. When processing a GEP chain, only the first ILM in the chain
  // matters so the fact that one of these offsets may get used in multiple
  // places doesn't matter
  ILM ilm;

public:
  Offset(int ilix, int64_t offset, int prev, const ILM& ilm, bool smove = false)
      : ilix(ilix), sptr(SPTR_NULL), offset(offset), prev(prev), ignore(false),
        smove(smove), ilm(ilm) {
    switch(ilm.opc) {
    case IM_ELEMENT:
    case IM_SUBS:
    case IM_SST:
    case IM_NSST:
    case IM_PDFUNCA: // XXX: Not sure if this is correct
      ignore = true;
      break;
    default:
      break;
    }
  }

  Offset(int ilix, SPTR sptr, int64_t offset)
      : ilix(ilix), sptr(sptr), offset(offset), ignore(false), ilm(-1, N_ILM) {
    ;
  }
};

// If this function has an internal function (parent) or is an internal
// function (child), the closure that contains the parent's variables that
// will be passed from parent to child
//
class Closure {
private:
  std::string llvm;
  std::vector<Component<Closure>> components;
  std::map<SPTR, unsigned> cmap;

public:
  Closure(const std::string& llvm = "") : llvm(llvm) {
    ;
  }

  void setLLVM(const std::string& llvm) {
    this->llvm = llvm;
  }

  // Only used when the closure has no arguments. Flang adds a bogus argument
  // to prevent the struct from being treated as opaque. So naturally, we need
  // to deal with that ridiculous decision (ridiculous because LLVM does allow
  // "empty" structs that are not opaque
  void add() {
    if(llvm.substr(0, 7) == "struct.")
      components.emplace_back(SPTR_NULL, llvm.substr(7), "i8*", false, false);
    else
      components.emplace_back(SPTR_NULL, llvm, "i8*", false, false);
  }

  void add(SPTR sptr, const std::string& type, bool pointer) {
    components.emplace_back(
        sptr, SYMNAME(sptr), type, pointer, is_array_desc(sptr));
    cmap[sptr] = components.size() - 1;
  }

  const std::string& getLLVM() const {
    return llvm;
  }

  bool isComponent(SPTR sptr) const {
    return cmap.find(sptr) != cmap.end();
  }

  const std::vector<Component<Closure>>& getComponents() const {
    return components;
  }

  const Component<Closure>& getComponent(unsigned i) const {
    return components.at(i);
  }
};

// Keeps track of everything needed for the current function
class Function {
private:
  // We only really need the SPTR and the name because the function may be
  // a parent for a contained function and the fewer global variables to
  // keep track of, the better, so rather than having the parent living
  // globally, we keep it here
  SPTR sptr;
  std::string llvmName;
  std::string srcName;

  bool hasIntrnal;
  bool isIntrnal;
  bool processed;

  // A copy of datatypexfer which is static in upper.c
  std::vector<DTYPE> datatypeMap;

  // A copy of symbolxfer which is static in upper.c
  std::vector<SPTR> symbolMap;

  // The offsets computed for use in the function. These will be used in the
  // GEP instructions
  std::map<int, Offset> offsets;

  // The "final" offsets into some real or imagined global. The offsets
  // themselves have a reference to the final SPTR tht is used, but we can
  // do it with the ili instructions instead because that feels better
  std::map<int, Offset> finals;

  // These are only AADD or ASUB instructions. I don't know if ASUB will
  // ever show up, but if it does, it will go here
  std::map<int, ILI> ilis;

  // The GEP instructions in this function. This will include both GEP's
  // with constant and non-constant indices. The order in this array will
  // directly correspond with the order in which they appear in the LLVM IR
  std::vector<GEP> geps;

  // The current ILM instruction being processed. We need this to correctly
  // determine the OffsetKind when we encounter it
  std::deque<ILM> ilms;

  // Just when you think it can't get any stupider!
  // The module-level variables in Fortran are kept around as byte-arrays
  // (but naturally, why would anyone NOT do it that way?).
  // But since just having byte arrays isn't really fun, it also splits it
  // up into categories depending on the type of the variable, it's
  // visibility whether or not it is initialized and where it lives
  // (host/device). Each combination of visibility, type, initialization
  // status, and residence has a unique index and the name of the global
  // variable reflects this index
  //
  // The table below is taken from flang1/flang1exe/module.c
  //
  //  0: struct/int/real: PUBLIC
  //  1: char             PUBLIC
  //  2: long             PUBLIC
  //  3: *                PUBLIC, THREAD_LOCAL
  //  4: struct/int/real: PRIVATE
  //  5: char             PRIVATE
  //  6: long             PRIVATE
  //  7: *                PRIVATE, THREAD_LOCAL
  //  8: struct/int/real: INIT, PUBLIC
  //  9: char             INIT, PUBLIC
  // 10: long             INIT, PUBLIC
  // 11: *                INIT, PUBLIC, THREAD_LOCAL
  // 12: struct/int/real: INIT, PRIVATE
  // 13: char             INIT, PRIVATE
  // 14: long             INIT, PRIVATE
  // 15: *                INIT, PRIVATE, THREAD_LOCAL
  //
  // It is incomplete because I have not bothered with the indices for
  // device-specific code because that will never happen in my case.
  //
  // The reason this matters is because - and by this point you should not
  // find any of the following surprising in the slightest - when generating
  // the instructions to look up the module level variables, if looking up
  // the character types, the "final" offset does not reflect the offset of
  // the start of the string in the global, but rather the offset to
  // whatever element of the string is being looked up.
  //
  // For instance, consider this module
  //
  // module test
  //   type mytype
  //     real :: pad
  //     integer :: data(3)
  //   end type mytype
  //
  //   integer :: gi(4)
  //   type(mytype) :: gt
  //   character(19) :: s, t
  //
  // end module test
  //
  // Accessing gt % data(2), might lead to a chain of offsets as follows
  //
  //   offset: 8  (within data)
  //   offset: 4  (within mt)
  //   final: 16  (within the global representing struct-type module-level
  //   variables)
  //
  // On the other hand, accessing t(3) would result in this
  //
  //   offset: 3 (within t)
  //   final: 21 (within the global representing char-type module-level
  //   variables)
  //
  // 21 in this case is the offset to t(3) and the only way of getting the
  // adjustment to the start of the string is to know that this is a
  // true byte array and special case it
  //
  // The map here is from SPTR to the index which indicates what "category"
  // of module-level variable it is
  //
  std::map<SPTR, const Component<FakeGlobal>*> fakeModuleGlobalComponents;

  // This also includes the BSS and STATICS variables for each function
  // Only the Flying Spaghetti Monster (RAAAMEN!) in it's infinite wisdom
  // knows what those are for. This is separate from the module globals because
  // there is a lovely case where, because Flang treats the module as a
  // "function", the same SPTR can occur in both fake function globals and
  // fake module globals (since the "function" and the "module" happen to
  // be one and the same in that case. But I can't find a sane way of
  // distinguishing between fake functions and real ones early in the process,
  // so I have to separate them here)
  //
  std::map<SPTR, const Component<FakeGlobal>*> fakeFunctionGlobalComponents;

  // The globals themselves
  std::map<SPTR, FakeGlobal> fakeGlobals;

  // If this is an internal function or has internal functions, the elements
  // of the closure that is passed from parent to child
  Closure closure;

private:
  void processFakes(bool);
  void process(GEP& gep, int ilix);
  void process(GEP& gep);
  void addSdscOffset(int ilix, int64_t offset, int64_t base, Offset& off);

  void addFakeGlobal(const FakeGlobal&,
                     std::map<SPTR, const Component<FakeGlobal>*>&);
  bool isFakeGlobalComponent(SPTR sptr) const;
  bool isFakeFunctionGlobalComponent(SPTR sptr) const;
  bool isFakeModuleGlobalComponent(SPTR sptr) const;
  bool isClosureComponent(SPTR sptr) const;
  const Component<FakeGlobal>& getFakeGlobalComponent(SPTR sptr) const;

public:
  Function();
  virtual ~Function() = default;

  void setSptr(SPTR sptr);
  void setLLVMName(const std::string& name);
  void setSourceName(const std::string& module, const std::string& name);
  void setIsInternal(bool isInternal);
  void setHasInternal(bool hasInternal);
  void setDatatypeMap(const DTYPE* types, uint64_t count);
  void setSymbolMap(const SPTR* sptrs, uint64_t count);
  void addOffset(int ilix, int64_t offset, int prev);
  void addSdscOffset(int ilix, int acon, int64_t offset, int64_t base);
  void addStrOffset(int curr, int actual);
  void addFinal(int ilix, SPTR sptr, int64_t offset);
  void addGEP(int ilix, int op1, int op2);
  void addILI(int ilix, int op1, int op2);
  void
  addFakeFunctionGlobal(SPTR sptr,
                        const std::string& name,
                        const std::string& type,
                        FakeGlobal::Kind kind,
                        const std::string& parent,
                        const std::vector<Component<FakeGlobal>>& components);
  void
  addFakeModuleGlobal(SPTR sptr,
                      const std::string& name,
                      const std::string& type,
                      FakeGlobal::Kind kind,
                      const std::string& parent,
                      const std::vector<Component<FakeGlobal>>& components);

  void addClosure(const std::string& llvm);
  void addToClosure();
  void addToClosure(SPTR component, const std::string& type, bool pointer);
  void addDescriptorAlloca(const std::string& alloca, const std::string& base);
  void beginILM(int ilm, ILM_OP opc);
  void endILM(int ilm);

  SPTR getPtr() const;
  const std::string& getLLVMName() const;
  const std::string& getSourceName() const;
  bool isInternal() const;
  bool hasInternal() const;
  bool isProcessed() const;
  uint64_t getILMDepth() const;

  void process(SPTR func_sptr);
  void process(SPTR func_sptr,
               const LL_ABI_Info& abi_info,
               const LL_Function& llvmFunc,
               LL_Module& module);
};

Function::Function()
    : sptr(SPTR_NULL), llvmName(""), srcName(""), hasIntrnal(false),
      isIntrnal(false), processed(false) {
  ;
}

SPTR Function::getPtr() const {
  return sptr;
}

const std::string& Function::getLLVMName() const {
  return llvmName;
}

const std::string& Function::getSourceName() const {
  return srcName;
}

bool Function::isInternal() const {
  return isIntrnal;
}

bool Function::hasInternal() const {
  return hasIntrnal;
}

bool Function::isProcessed() const {
  return processed;
}

bool Function::isClosureComponent(SPTR sptr) const {
  return closure.isComponent(sptr);
}

bool Function::isFakeGlobalComponent(SPTR sptr) const {
  return isFakeModuleGlobalComponent(sptr)
         or isFakeFunctionGlobalComponent(sptr);
}

bool Function::isFakeModuleGlobalComponent(SPTR sptr) const {
  return fakeModuleGlobalComponents.find(sptr)
         != fakeModuleGlobalComponents.end();
}

bool Function::isFakeFunctionGlobalComponent(SPTR sptr) const {
  return fakeFunctionGlobalComponents.find(sptr)
         != fakeFunctionGlobalComponents.end();
}

const Component<FakeGlobal>& Function::getFakeGlobalComponent(SPTR sptr) const {
  if(isFakeFunctionGlobalComponent(sptr))
    return *fakeFunctionGlobalComponents.at(sptr);
  else if(isFakeModuleGlobalComponent(sptr))
    return *fakeModuleGlobalComponents.at(sptr);

  // Suppress return value warning
  moya_error("Not a fake global component: %d\n", sptr);
  return *static_cast<const Component<FakeGlobal>*>((void*)0x1);
}

uint64_t Function::getILMDepth() const {
  return ilms.size();
}

void Function::setSptr(SPTR sptr) {
  this->sptr = sptr;
}

void Function::setLLVMName(const std::string& name) {
  this->llvmName = name;
}

void Function::setSourceName(const std::string& module,
                             const std::string& name) {
  std::stringstream ss;
  if(module.length())
    ss << module << ".";
  ss << name;
  this->srcName = ss.str();
}

void Function::setIsInternal(bool isInternal) {
  this->isIntrnal = isInternal;
}

void Function::setHasInternal(bool hasInternal) {
  this->hasIntrnal = hasInternal;
}

void Function::beginILM(int ilm, ILM_OP op) {
  ilms.emplace_back(ilm, op, ilms.size());
}

void Function::endILM(int ilm) {
  if(ilms.back().ilm != ilm)
    moya_error(
        "Inconsistent ILMs. Expected %d. Got %d\n", ilm, ilms.back().ilm);
  ilms.pop_back();
}

void Function::addOffset(int ilix, int64_t offset, int prev) {
  // For some reason, rm_smove also inserts instructions and this is after
  // all the ILM's have been processed. I am sure that there is a "good"
  // reason for this, as there is for all of this piece of decomposing
  // roadkill. In any case, I need the ILM's to be able to do anything
  // correctly and I am not sure if any of the code that rm_smove adds makes
  // it into the LLVM anyway, so jsut ignore them
  if(not ilms.size())
    return;

  if(offsets.find(ilix) != offsets.end()) {
    const Offset& off = offsets.at(ilix);
    // The only way this will be allowed is if an offset instruction without
    // a previous set is overwritten with one with a previous.
    if(off.prev)
      moya_error("Offset already in map. (%d, %ld) => (%d, %ld)\n",
                 off.prev,
                 off.offset,
                 prev,
                 offset);
    else if(prev)
      offsets.erase(offsets.find(ilix));
  } else if(not ilms.size()) {
    moya_error("No active ILM instruction\n");
  }

  const ILM& ilm = ilms.back();

  // Because we don't allow Flang to ignore 0-valued ACON, because of a
  // quirk of how these are constructed, we have "double" indices for an
  // array reference. At least one of the offsets will be zero.
  // Catch that special case here so we don't have to catch
  // it during GEP processing
  if(offsets.find(prev) != offsets.end()
     and (offset == 0 or offsets.at(prev).offset == 0)
     and (offsets.at(prev).ilm.ilm == ilms.back().ilm)
     and offsets.at(prev).prev) {
    const Offset& off = offsets.at(prev);
    offsets.emplace(std::make_pair(
        ilix, Offset(ilix, off.offset ? off.offset : offset, off.prev, ilm)));
    offsets.erase(prev);
  } else if((ilm.opc == IM_ELEMENT) and (ilm.depth == 1)
            and (ilms.front().opc == IM_SMOVE)) {
    // When structs are being moved, a
    // pointer to the struct is passed to memcpy(). If the structs are in
    // an array, the offset calculations will have a single array element
    // indicator. But the PropagateAnalysisTypes pass will see the struct
    // and attempt to find the element inside it and fail because it will
    // be expecting something other than an array element indicator. In that
    // case, set a flag indicating that this GEP is not a struct element
    // lookup
    //
    offsets.emplace(
        std::make_pair(ilix, Offset(ilix, offset, prev, ilm, true)));
    offsets.at(ilix).smove = true;
  } else {
    offsets.emplace(std::make_pair(ilix, Offset(ilix, offset, prev, ilm)));
  }
}

void Function::addSdscOffset(int ilix,
                             int64_t offset,
                             int64_t base,
                             Offset& off) {
  if(off.offset != offset)
    moya_error("Inconsistent offsets in array descriptor lookup:\n"
               "  ilix: %d\n"
               "  acon: %d\n"
               "   old: %ld\n"
               "   new: %ld\n",
               ilix,
               off.ilix,
               off.offset,
               offset);

  offsets.emplace(
      std::make_pair(-ilix, Offset(-ilix, base, off.prev, ILM(-1, IM_MEMBER))));
  off.offset = offset - base;
  off.prev = -ilix;
}

// Will the wonders never cease!
//
// get_sdsc_element() looks up an element of the array descriptor
// inside a struct by directly computing the offset to it. For some reason,
// the regular sequence of steps computing the address aren't there. So we
// add a "fake" step to get the offset to the descriptor within the struct
//
void Function::addSdscOffset(int ilix, int acon, int64_t offset, int64_t base) {
  // For some reason, rm_smove also inserts instructions and this is after
  // all the ILM's have been processed. I am sure that there is a "good"
  // reason for this, as there is for all of this piece of decomposing
  // roadkill. In any case, I need the ILM's to be able to do anything
  // correctly and I am not sure if any of the code that rm_smove adds makes
  // it into the LLVM anyway, so jsut ignore them
  if(not ilms.size())
    return;

  if(offsets.find(acon) != offsets.end()) {
    addSdscOffset(ilix, offset, base, offsets.at(acon));
  } else if(ilis.find(ilix) == ilis.end()) {
    // If an AADD for the instruction is not present, there should be an
    // offset declared for it
    if(offsets.find(ilix) == offsets.end())
      moya_error("Expected offset in array descriptor lookup:\n"
                 "  ilix: %d\n"
                 "  acon: %d\n",
                 ilix,
                 acon);

    addSdscOffset(ilix, offset, base, offsets.at(ilix));
  } else {
    // The pattern seems to be:
    //
    // ptr_op = AADD(ptr_op, ptr_acon)
    // ilix = AADD(ptr_op, acon)
    //
    // An offset for ptr_acon should already have been registered
    //
    const ILI& ili = ilis.at(ilix);
    if(ilis.find(ili.op1) == ilis.end()) {
      moya_error("Expected ACON instruction in array descriptor lookup:\n"
                 "  ilix: %d\n"
                 "  acon: %d\n",
                 ilix,
                 acon);
    }

    const ILI& ptr_ili = ilis.at(ili.op1);
    if(offsets.find(ptr_ili.op2) == offsets.end()) {
      moya_error("Expected ACON offset in array descriptor lookup:\n"
                 "  ilix: %d\n"
                 "  acon: %d\n",
                 ilix,
                 ptr_ili.op2);
    }

    addSdscOffset(ilix, offset, base, offsets.at(ptr_ili.op2));
  }
}

// Oh boy! Here we go again!
//
// Strings elements/substrings are looked up by directly computing the
// offset to them even if they are within structs. Once again, we have to
// special case this. The actual chain of offsets to get to the right
// element have been computed and we need to hook up to it
//
void Function::addStrOffset(int curr, int actual) {
  if(offsets.find(curr) == offsets.end()) {
    // The ACON instruction of the current offset should already have been
    // added
    moya_error("Missing current offset in string lookup: %d\n", curr);
  } else if(offsets.find(actual) == offsets.end()) {
    // ???: I don't know if this is correct. But when using temporary
    // strings from storechartmp(), actual also doesn't exist. But for
    // temporary strings, the offset should always be zero because they are
    // local variables that get set
    if(offsets.at(curr).offset)
      moya_error("Missing actual offset in string lookup: %d\n", actual);
    else
      moya_print("WARNING: Missing actual offset: %d[%ld]\n",
                 actual,
                 offsets.at(curr).offset);
    return;
  }

  Offset& offCurr = offsets.at(curr);
  const Offset& offActual = offsets.at(actual);

  // Overlay the values of actual onto the current one
  // We don't create a link to the actual value because when processing the
  // GEP it will result in an additional index which will not be correct
  offCurr.prev = offActual.prev;
  offCurr.offset = offActual.offset;
  offCurr.ignore = offActual.ignore;
  offCurr.ilm = offActual.ilm;
}

void Function::addFinal(int ilix, SPTR sptr, int64_t offset) {
  if(finals.find(ilix) != finals.end()) {
    int64_t finl = finals.at(ilix).offset;
    if(finl != offset)
      moya_error("Final already in map: %d => %d", finl, offset);
  }
  finals.emplace(std::make_pair(ilix, Offset(ilix, sptr, offset)));
}

void Function::addGEP(int ilix, int op1, int op2) {
  geps.emplace_back(ilix, op1, op2);
}

void Function::addILI(int ilix, int op1, int op2) {
  ilis.emplace(std::make_pair(ilix, ILI(ilix, op1, op2)));
}

void Function::addFakeGlobal(
    const FakeGlobal& fake,
    std::map<SPTR, const Component<FakeGlobal>*>& cmap) {
  for(const Component<FakeGlobal>& component : fake.getComponents()) {
    if(cmap.find(component.getSptr()) != cmap.end())
      moya_error("Duplicate fake global component: %d (%s, %s)\n",
                 component.getSptr(),
                 SYMNAME(component.getSptr()),
                 fake.getName().c_str());
    cmap[component.getSptr()] = &component;
  }
}

void Function::addFakeModuleGlobal(
    SPTR sptr,
    const std::string& name,
    const std::string& type,
    FakeGlobal::Kind kind,
    const std::string& parent,
    const std::vector<Component<FakeGlobal>>& components) {
  if(fakeGlobals.find(sptr) != fakeGlobals.end())
    moya_error("Duplicate fake module global: %d (%s)\n", sptr, name.c_str());
  else if(not FakeGlobal::isModuleGlobal(kind))
    moya_error("Invalid kind for fake module global: %d (%s) => %d\n",
               sptr,
               name.c_str(),
               kind);
  fakeGlobals.emplace(sptr,
                      FakeGlobal(sptr, name, type, kind, parent, components));
  addFakeGlobal(fakeGlobals.at(sptr), fakeModuleGlobalComponents);
}

void Function::addFakeFunctionGlobal(
    SPTR sptr,
    const std::string& name,
    const std::string& type,
    FakeGlobal::Kind kind,
    const std::string& parent,
    const std::vector<Component<FakeGlobal>>& components) {
  if(fakeGlobals.find(sptr) != fakeGlobals.end())
    moya_error("Duplicate fake function global: %d (%s)\n", sptr, name.c_str());
  else if(not FakeGlobal::isFunctionGlobal(kind))
    moya_error("Invalid kind for fake function global: %d (%s) => %d\n",
               sptr,
               name.c_str(),
               kind);
  moya_iprint("addFakeFunctionGlobal: %d => %s\n", sptr, name.c_str());

  fakeGlobals.emplace(sptr,
                      FakeGlobal(sptr, name, type, kind, parent, components));
  addFakeGlobal(fakeGlobals.at(sptr), fakeFunctionGlobalComponents);
}

void Function::addClosure(const std::string& llvm) {
  closure.setLLVM(llvm);
}

void Function::addToClosure() {
  closure.add();
}

void Function::addToClosure(SPTR sptr, const std::string& type, bool pointer) {
  closure.add(sptr, type, pointer);
}

void Function::setDatatypeMap(const DTYPE* dtypes, uint64_t count) {
  datatypeMap.assign(dtypes, &dtypes[count]);
}

void Function::setSymbolMap(const SPTR* sptrs, uint64_t count) {
  symbolMap.assign(sptrs, &sptrs[count]);
}

void Function::process(GEP& gep, int ilix) {
  if(offsets.find(ilix) != offsets.end()) {
    Offset& offset = offsets.at(ilix);
    if(offset.prev) {
      // This is part of a chain and there will be another offset declaration
      // to be found if we follow it
      gep.addOffset(offset.offset, offset.ignore);
      process(gep, offset.prev);
    } else if(finals.find(gep.ilix) != finals.end()) {
      // If this is the last element in the chain, we could still be looking
      // up some (real or imagined) global. In that case, there will be an
      // additional final offset declaration.

      const Offset& finl = finals.at(gep.ilix);
      if(isFakeModuleGlobalComponent(finl.sptr)
         and (getFakeGlobalComponent(finl.sptr).getParent().isChar())) {
        gep.addOffset(getFakeGlobalComponent(finl.sptr).getOffset(), false);
      } else if(isFakeFunctionGlobalComponent(finl.sptr)) {
        gep.addOffset(getFakeGlobalComponent(finl.sptr).getOffset(), false);
      } else if(isClosureComponent(finl.sptr)) {
        gep.addOffset(finl.offset, false);
      } else {
        // Sanity check. If there is a final offset, this one should be zero
        // EXCEPT (and by this point, you should expect an exception), if
        // the instruction is a BOS. Why? Because this seems to be done
        // when a function has an inner function and the closure needs to
        // be set correctly. In this case, there is a single instruction
        // with the offset into the closure struct and no chain
        //
        if(offset.ilm.opc != IM_BOS and offset.offset)
          moya_error("Expected zero offset for final element in chain\n"
                     "    ilix: %d\n"
                     "  offset: %ld\n",
                     gep.ilix,
                     offset.offset);

        // This is a surreal nightmare!
        //
        //    “Forward, the Light Brigade!”
        //    Was there a man dismayed?
        //    Not though the soldier knew
        //    Someone had blundered.
        //
        // The offsets for strings and arrays work slightly differently for
        // certain cases.
        //
        //  - If the string is a local variable (saved or otherwise), the
        //  final
        //    offset
        //    is always the offset (possibly non-constant) into the string.
        //    The offset will likely already have been added in which case we
        //    won't bother adding the final
        //
        //  - If the array is a local variable, then when looking up the array
        //    descriptor, we get an offet into the descriptor as the final
        //    offset and it is always zero. Obviously, it should be ignored
        //    because the actual offset will already have been captured
        //    earlier
        //
        SPTR sptr = finl.sptr;
        if(is_local(sptr) and is_string(sptr))
          ;
        else if(is_local(sptr) and is_array(sptr))
          ;
        else
          gep.addOffset(finals.at(gep.ilix).offset, false);
      }
    } else {
      // If there is no final declaration, treat it like a normal offset
      // ... AAAAAAAAND ... WAIT FOR IT ... (with glorious brass fanfare)
      // with a special case.
      //
      gep.addOffset(offset.offset, offset.ignore);

      // This special case is almost certainly a hack, but I can't think of a
      // better way of dealing with this. Explanation in Funcion::addOffset()
      if(offset.smove)
        gep.array = true;
    }
  }
}

void Function::process(GEP& gep) {
  if(ilis.find(gep.ilix) != ilis.end()) {
    process(gep, gep.op2);
  } else if(offsets.find(gep.ilix) != offsets.end()) {
    process(gep, gep.ilix);
  } else {
    moya_error("Cannot process GEP: %d: %d %d\n", gep.ilix, gep.op1, gep.op2);
  }
  // The offsets that will have been collected will be in reverse order. So
  // get everything nice and tidy as we need it. Also sanity check the results
  gep.finalize();
}

void Function::processFakes(bool isModule) {
  moya_iprint("fakes\n");
  for(const auto& it : fakeGlobals) {
    const FakeGlobal& g = it.second;

    std::vector<const char*> names;
    std::vector<unsigned> offsets;
    std::vector<const char*> types;
    std::vector<unsigned> descriptors;

    moya_iprint(
        "  struct %s [%s]\n", g.getName().c_str(), g.getParent().c_str());
    for(const Component<FakeGlobal>& component : g.getComponents()) {
      names.push_back(component.getName().c_str());
      offsets.push_back(component.getOffset());
      types.push_back(component.getType().c_str());
      descriptors.push_back(component.getDescriptor());
      moya_iprint("    %6d: %s => %s\n",
                  component.getOffset(),
                  component.getName().c_str(),
                  component.getType().c_str());
    }

    std::string source(getSourceName());
    std::string parent(getLLVMName());
    if(isModule) {
      if(FakeGlobal::isFunctionGlobal(g.getKind())) {
        switch(g.getKind()) {
        case FakeGlobal::Kind::FunctionBSS:
          source.append(".bss");
          break;
        case FakeGlobal::Kind::FunctionStatics:
          source.append(".static");
          break;
        default:
          moya_error("Unknown function global kind: %d\n", g.getKind());
          break;
        }
      } else if(FakeGlobal::isModuleGlobal(g.getKind())) {
        if(g.getKind() == FakeGlobal::Kind::Unknown) {
          source.clear();
          parent.clear();
        } else {
          if(FakeGlobal::isPublic(g.getKind())) {
            source.append(".pub");
          } else if(FakeGlobal::isPrivate(g.getKind())) {
            source.append(".priv");
          } else {
            parent.clear();
            source.clear();
          }
          if(not source.empty()) {
            if(FakeGlobal::isInit(g.getKind()))
              source.append(".init");
            else
              source.append(".auto");
            if(FakeGlobal::isThreadLocal(g.getKind()))
              source.append(".tl");
            else if(FakeGlobal::is8(g.getKind()))
              source.append(".8");
            else if(FakeGlobal::is64(g.getKind()))
              source.append(".64");
            else
              source.append(".def");
          }
        }
      }
    } else {
      if(FakeGlobal::isFunctionGlobal(g.getKind())) {
        switch(g.getKind()) {
        case FakeGlobal::Kind::FunctionBSS:
          source.append(".bss");
          break;
        case FakeGlobal::Kind::FunctionStatics:
          source.append(".static");
          break;
        default:
          moya_error("Unknown function global kind: %d\n", g.getKind());
          break;
        }
      } else {
        source.clear();
        parent.clear();
      }
    }

    moya_module_add_global(g.getName().c_str(),
                           source.c_str(),
                           g.getType().c_str(),
                           parent.c_str(),
                           g.getComponents().size(),
                           names.data(),
                           offsets.data(),
                           types.data(),
                           descriptors.data());
  }
}

void Function::process(SPTR func_sptr) {
  std::string path = flang_curr_get_file();

  setSptr(func_sptr);
  setLLVMName(SYMNAME(func_sptr));
  setSourceName("", SYMNAME(func_sptr));

  moya_iprint("MODULE: %s\n", SYMNAME(func_sptr));
  moya_module_add_fmodule(
      getLLVMName().c_str(), path.c_str(), FUNCLINEG(func_sptr), 0);
  processFakes(true);
}

void Function::process(SPTR func_sptr,
                       const LL_ABI_Info& abi,
                       const LL_Function& llvmFunc,
                       LL_Module& llvmModule) {
  unsigned nargs = abi.nargs;
  std::string path = flang_curr_get_file();

  setSptr(func_sptr);
  setLLVMName(get_llvm_name(func_sptr));
  setSourceName(SYMNAME(INMODULEG(func_sptr)), SYMNAME(func_sptr));
  setIsInternal(flang_curr_is_internal());
  setHasInternal(flang_curr_has_internal());
  processed = true;

  moya_module_add_function(getLLVMName().c_str(),
                           getSourceName().c_str(),
                           SYMNAME(func_sptr),
                           path.c_str(),
                           FUNCLINEG(func_sptr),
                           0,
                           isInternal());

  std::map<int, int> links;
  std::map<SPTR, SPTR> dummies;
  std::map<SPTR, int> argnos;
  std::vector<bool> arrays(nargs, false);
  moya_iprint("FUNCTION (%d): %s\n", func_sptr, get_llvm_name(func_sptr));
  for(unsigned i = 0; i < nargs; i++) {
    SPTR arg = abi.arg[i + 1].sptr;
    if(SPTR linked = SDSCG(arg)) {
      dummies[linked] = arg;
      if(is_array_desc(linked))
        arrays[i] = true;
    }
    argnos[arg] = i;
    links[i] = -1;
  }

  for(auto& it : dummies) {
    if(it.second != -1) {
      links[argnos[it.first]] = argnos[it.second];
      links[argnos[it.second]] = argnos[it.first];
    }
  }

  for(unsigned i = 0; i < nargs; i++) {
    bool closure = false;
    SPTR arg = abi.arg[i + 1].sptr;
    std::string type = make_lltype_from_dtype(DTYPEG(arg))->str;
    if(isInternal() and (i == nargs - 1)) {
      // The last argument of an internal function is a closure.
      //
      // FIXME: Yes, this is absolutely terrible, because it can get messed up
      // if the naming is changed, but flang is absolutely putrid anyway, so
      // why not. The correct closure type will get set once the types are
      // instrumented.
      //
      std::stringstream ss;
      ss << "%struct.struct_ul_" << func_get_parent().getLLVMName() << "_"
         << func_get_parent().getPtr() << "*";
      type = ss.str();
      closure = true;
    } else if(IS_INTERFACEG(arg)) {
      std::stringstream ss;
      unsigned count
          = moya_module_get_num_interface_params(getLLVMName().c_str(), i);
      const SPTR* params = reinterpret_cast<const SPTR*>(
          moya_module_get_interface_params(getLLVMName().c_str(), i));
      ss << make_lltype_from_dtype(DTYPEG(arg))->str << " (";
      for(int j = 0; j < count; j++) {
        SPTR param = symbolMap[params[j]];
        DTYPE dtype = DTYPEG(param);
        ss << make_lltype_from_dtype(dtype)->str;
        if(not PASSBYVALG(param))
          ss << "*";
        if(j < count - 1)
          ss << ", ";
      }
      ss << ")*";
      type = ss.str();
    } else if(not PASSBYVALG(arg)) {
      type.append("*");
    }
    if(POINTERG(arg))
      type.append("*");

    moya_iprint("  %2d: %d %s [%d] => %s\n",
                i,
                arg,
                SYMNAME(arg),
                STYPEG(arg),
                type.c_str());
    moya_function_add_arg(getLLVMName().c_str(),
                          SYMNAME(arg),
                          type.c_str(),
                          links[i],
                          dummies.find(arg) != dummies.end(),
                          arrays[i],
                          closure);
  }

  if(int64_t min
     = moya_module_get_min_allocated_type_id(getLLVMName().c_str())) {
    int64_t max = moya_module_get_max_allocated_type_id(getLLVMName().c_str());
    for(int64_t id = min; id <= max; id++) {
      int type = moya_module_get_allocated_type(getLLVMName().c_str(), id);
      DTYPE dtype = datatypeMap[type];
      moya_function_add_allocated_type(
          getLLVMName().c_str(), id, make_lltype_from_dtype(dtype)->str);
    }
  }

  moya_iprint("geps\n");
  unsigned i = 1;
  for(GEP& gep : geps) {
    process(gep);
    moya_iprint("  %d => %d: [", i++, gep.ilix);
    for(int64_t offset : gep.offsets)
      moya_print(" %ld", offset);
    moya_print(" ] = %ld [%ld] %d\n", gep.offset, gep.cumulative, gep.array);
    moya_function_add_gep(getLLVMName().c_str(),
                          gep.cumulative,
                          gep.offset,
                          gep.array,
                          gep.offsets.data(),
                          gep.offsets.size());
  }

  if(closure.getLLVM().size()) {
    moya_module_add_closure(closure.getLLVM().c_str(), getLLVMName().c_str());
    for(const Component<Closure>& component : closure.getComponents()) {
      moya_closure_add_type(closure.getLLVM().c_str(),
                            component.getName().c_str(),
                            component.getType().c_str(),
                            component.getPointer(),
                            component.getDescriptor());
    }
  }

  moya_iprint("locals\n");
  for(const LL_Object* object = llvmFunc.first_local; object;
      object = object->next) {
    std::string alloca = object->address.data;
    if(alloca.length() and alloca[0] == '%')
      alloca = alloca.substr(1);
    moya_iprint("  %s => %s\n", SYMNAME(object->sptr), alloca.c_str());
    moya_module_add_local_array_2(
        getLLVMName().c_str(), SYMNAME(object->sptr), alloca.c_str());
  }

  processFakes(false);
}

} // namespace

// GLOBAL VARIABLES

static std::deque<Function> g_currfn;
static Module g_module;

// Flang Interface functions

static Module& module_get() {
  return g_module;
}

static Function& func_get() {
  return g_currfn.front();
}

static Function& func_get_parent() {
  return g_currfn.at(1);
}

static Function& func_add() {
  g_currfn.emplace_front();
  return func_get();
}

static void func_remove() {
  g_currfn.pop_front();
}

static bool func_added() {
  return g_currfn.size();
}

static void func_parent_remove() {
  if(g_currfn.size() < 2)
    moya_error("No parent on stack");
  else if(not g_currfn.at(1).hasInternal())
    moya_error("Parent function not correctly initialized");
  g_currfn.erase(g_currfn.begin() + 1);
}

static bool func_parent_added() {
  if(g_currfn.size() > 1 and g_currfn.at(1).hasInternal())
    return true;
  return false;
}

void moya_indent() {
  if(not moya_enabled())
    return;

  if(func_added())
    for(int i = 0; i < func_get().getILMDepth(); i++)
      fprintf(stderr, "  ");
}

void moya_init(const char* cmdline, const char* ilmfile) {
  if(moya_init_lib(cmdline)) {
    INIT_API_FUNC(moya_module_initialize);
    INIT_API_FUNC(moya_module_add_fmodule);
    INIT_API_FUNC(moya_module_add_layout_descriptor);
    INIT_API_FUNC(moya_module_add_closure);
    INIT_API_FUNC(moya_module_add_function);
    INIT_API_FUNC(moya_module_add_global);
    INIT_API_FUNC(moya_module_get_min_allocated_type_id);
    INIT_API_FUNC(moya_module_get_max_allocated_type_id);
    INIT_API_FUNC(moya_module_get_allocated_type);
    INIT_API_FUNC(moya_module_is_global_category);
    INIT_API_FUNC(moya_module_get_global_category_module);
    INIT_API_FUNC(moya_module_add_local_array_2);
    INIT_API_FUNC(moya_module_serialize);
    INIT_API_FUNC(moya_closure_add_type);
    INIT_API_FUNC(moya_function_add_arg);
    INIT_API_FUNC(moya_function_add_child);
    INIT_API_FUNC(moya_function_add_allocated_type);
    INIT_API_FUNC(moya_function_add_gep);
    INIT_API_FUNC(moya_module_add_struct);
    INIT_API_FUNC(moya_module_add_typedesc);
    INIT_API_FUNC(moya_module_get_num_interface_params);
    INIT_API_FUNC(moya_module_get_interface_params);
    INIT_API_FUNC(moya_struct_add_type);
    INIT_API_FUNC(moya_const_get_array_index_marker);
  }

  if(moya_enabled()) {
    // Have to allocate this string otherwise the string will be destructed
    // after it returns .cstr() and that buffer will be freed
    //
    std::string json = moya_get_json_file(ilmfile);
    moya_module_initialize(-1, false, json.c_str());
  }
}

void moya_fini(const char* outfile) {
  if(not moya_enabled())
    return;

  moya_module_serialize();
}

void moya_func_begin(void) {
  if(not moya_enabled())
    return;

  moya_iprint("moya_func_begin: [%d]\n",
              func_added() ? func_get().hasInternal() : 0);
  func_add();
}

void moya_func_end(void) {
  if(not moya_enabled())
    return;

  moya_iprint("moya_func_end: %s [%d, %d, %d]\n",
              func_get().getLLVMName().c_str(),
              func_get().isProcessed(),
              func_get().hasInternal(),
              func_get().isInternal());
  if(not func_get().isProcessed())
    // This is the case for the "fake" functions that have the same name as
    // the module being processed
    func_remove();
  else if(not func_get().hasInternal())
    // If the function is an internal function or has no internal functions,
    // it is safe to remove
    func_remove();
}

void moya_func_set_name(SPTR func_sptr, const char* fname) {
  if(not moya_enabled())
    return;

  moya_iprint("func_set_name: %s => %s\n", SYMNAME(func_sptr), fname);
  if(func_added())
    func_get().setLLVMName(fname);
}

void moya_save_datatype_map(const DTYPE* dtypes, uint64_t count) {
  if(not moya_enabled())
    return;

  if(func_added())
    func_get().setDatatypeMap(dtypes, count);
}

void moya_save_symbol_map(const SPTR* sptrs, uint64_t count) {
  if(not moya_enabled())
    return;

  if(func_added())
    func_get().setSymbolMap(sptrs, count);
}

void moya_process_module(const LL_Module& llvmModule) {
  if(not moya_enabled())
    return;

  module_get().process(llvmModule);
}

void moya_process_fake_function(SPTR func_sptr) {
  if(not moya_enabled())
    return;

  // This will only get called when the module is processed as a subroutine.
  // (I am too exhausted to even come up with a sarcastic quip at this point)
  //
  func_get().process(func_sptr);
}

void moya_process_function(SPTR func_sptr,
                           const LL_ABI_Info& abi,
                           const LL_Function& llvmFunc,
                           LL_Module& llvmModule) {
  if(not moya_enabled())
    return;

  //   Theirs not to reason why,
  //   Theirs but to do and die.
  //   Into the valley of Death
  //   Rode the six hundred.
  //
  // Because of where the hooks are added, when a function is added to the
  // stack, it is not always correctly initialized. We don't know if it is an
  // internal function or has an internal function that we need to correctly
  // maintain the stack. If this function is not an internal function and
  // a "parent" function exists on the stack, then all children of the parent
  // have been processed and we need to get rid of it
  //
  // It sucks to have to do this
  // here but it's either do this or figure out a better place to put the
  // hooks in main.cpp. Given a choice between going back into Flang's source
  // code and doing crappy things here, take a wild guess what I'd rather do.
  //
  moya_iprint("process_function (%d): %s::%s\n",
              func_sptr,
              SYMNAME(INMODULEG(func_sptr)),
              SYMNAME(func_sptr));
  if(not flang_curr_is_internal() and func_parent_added())
    func_parent_remove();

  func_get().process(func_sptr, abi, llvmFunc, llvmModule);

  if(flang_curr_is_internal())
    moya_function_add_child(func_get_parent().getLLVMName().c_str(),
                            func_get().getLLVMName().c_str());
}

void moya_process_common_block(SPTR sptr, const char* name) {
  if(not moya_enabled())
    return;

  // The format of the block will be <modulename>_<category>_
  // FIXME: This will probably not work for actual Fortran 77 common blocks,
  // but I absolutely do not care about those right now
  //
  FakeGlobal::Kind kind = FakeGlobal::Kind::ModuleUnknown;
  std::string module = "";
  if(moya_module_is_global_category(SYMNAME(sptr))) {
    unsigned category = 0;
    for(unsigned i = strlen(name) - 2, mult = 1; name[i] != '_';
        i--, mult *= 10)
      category += (name[i] - '0') * mult;
    kind = static_cast<FakeGlobal::Kind>(category);
    module = moya_module_get_global_category_module(SYMNAME(sptr));
  }

  std::vector<Component<FakeGlobal>> components;
  moya_iprint("cmblk: %s (%d) => %s\n", name, sptr, module.c_str());
  for(SPTR cmem = CMEMFG(sptr); cmem > NOSYM; cmem = SYMLKG(cmem)) {
    LL_Type* type = make_lltype_from_dtype(DTYPEG(cmem));
    moya_iprint("  %d [%ld]: %s => %s\n",
                cmem,
                ADDRESSG(cmem),
                SYMNAME(cmem),
                type->str);
    components.emplace_back(
        cmem, SYMNAME(cmem), type->str, ADDRESSG(cmem), is_array_desc(cmem));
  }

  // The common blocks are still processed outside functions. Not quite sure
  // how that happens,
  if(func_added())
    func_get().addFakeModuleGlobal(
        sptr, name, std::string("struct") + name, kind, module, components);
}

void moya_process_fake_global(SPTR sptr, const char* gname, const char* fname) {
  if(not moya_enabled())
    return;

  FakeGlobal::Kind kind = FakeGlobal::Kind::Unknown;
  if(strstr(gname, ".BSS"))
    kind = FakeGlobal::Kind::FunctionBSS;
  else if(strstr(gname, ".STATICS"))
    kind = FakeGlobal::Kind::FunctionStatics;
  else
    moya_error("Unexpected fake global name: %s\n", gname);

  std::vector<Component<FakeGlobal>> components;
  moya_iprint("fake: %s (%s)\n", gname, fname);
  for(SPTR cmem = sptr; cmem > NOSYM; cmem = SYMLKG(cmem)) {
    std::string type = make_lltype_from_dtype(DTYPEG(cmem))->str;

    // Oh joy! For some reason, if it's a PLIST, the dtype is not an array
    // but a scalar and we need to "convert" it into an array. The same
    // thing is done in sym_is_refd() in flang1exe/assem.c, so who am I to
    // stick my nose up at something that has clearly been sanctified by the
    // powers that be
    //
    if(STYPEG(cmem) == ST_PLIST) {
      std::stringstream ss;
      ss << "[" << PLLENG(cmem) << " x " << type << "]";
      type = ss.str();
    }

    components.emplace_back(
        cmem, SYMNAME(cmem), type, ADDRESSG(cmem), is_array_desc(cmem));
    moya_iprint("  %d: %s [%ld] => %s\n",
                cmem,
                SYMNAME(cmem),
                ADDRESSG(cmem),
                type.c_str());
  }

  std::sort(components.begin(),
            components.end(),
            [](const Component<FakeGlobal>& left,
               const Component<FakeGlobal>& right) {
              return left.getOffset() < right.getOffset();
            });

  if(func_added())
    func_get().addFakeFunctionGlobal(
        sptr, gname, std::string("struct") + gname, kind, "", components);
}

void moya_process_closure(SPTR sptr, const char* tname, const char* fname) {
  if(not moya_enabled())
    return;

  std::stringstream ss;
  ss << "struct." << tname;
  std::string llvm = ss.str();

  moya_iprint("closure: %s (%s)\n", fname, tname);
  func_get().addClosure(llvm.c_str());
  unsigned numFields = 0;
  for(unsigned i = 0; i < AG_UPLEVEL_AVL(sptr); i++, numFields++) {
    SPTR field = AG_UPLEVEL_NEW(sptr, i);
    bool isPointer = AG_UPLEVEL_OLD(sptr, i);
    const char* type = make_lltype_from_dtype(DTYPEG(field))->str;
    moya_iprint("  %3d (%d): %s => %s\n",
                i,
                field,
                SYMNAME(field),
                type,
                isPointer ? "*" : "");
    func_get().addToClosure(field, type, isPointer);
  }
  if(not numFields)
    func_get().addToClosure();
}

void moya_global_add_layout_descriptor(const char* name) {
  if(not moya_enabled())
    return;

  module_get().addLayoutDescriptor(name);
}

void moya_inst_add_final(int ilix, SPTR sptr, int64_t offset) {
  if(not moya_enabled())
    return;

  func_get().addFinal(ilix, sptr, offset);
}

void moya_inst_add_gep(int ilix, int op1, int op2) {
  if(not moya_enabled())
    return;

  func_get().addGEP(ilix, op1, op2);
}

void moya_inst_add_ili(int ilix, int op1, int op2) {
  if(not moya_enabled())
    return;

  func_get().addILI(ilix, op1, op2);
}

void moya_inst_add_offset(int ilix, int64_t offset, int prev) {
  if(not moya_enabled())
    return;

  func_get().addOffset(ilix, offset, prev);
}

void moya_inst_add_sdsc_offset(int ilix,
                               int acon,
                               int64_t offset,
                               int64_t base) {
  if(not moya_enabled())
    return;

  func_get().addSdscOffset(ilix, acon, offset, base);
}

void moya_inst_add_str_offset(int curr, int actual) {
  if(not moya_enabled())
    return;

  func_get().addStrOffset(curr, actual);
}

void moya_type_add_typedesc(const char* name) {
  if(not moya_enabled())
    return;

  module_get().addTypeDesc(name);
}

void moya_ilm_begin(int ilm, ILM_OP opc) {
  if(not moya_enabled())
    return;

  func_get().beginILM(ilm, opc);
}

void moya_ilm_end(int ilm) {
  if(not moya_enabled())
    return;

  func_get().endILM(ilm);
}
