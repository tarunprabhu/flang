#ifndef FLANG2_MOYA_H_
#define FLANG2_MOYA_H_

#include "cgllvm.h"
#include "gbldefs.h"
#include "global.h"
#include "ll_structure.h"
#include "moyautil.h"
#include "symtab.h"

#include <stdio.h>

void moya_init(const char* cmdline, const char* ilmfile);
void moya_fini(const char* ilmfile);

void moya_save_datatype_map(const DTYPE* dtypes, uint64_t count);
void moya_save_symbol_map(const SPTR* sptrs, uint64_t count);

void moya_process_module(const LL_Module& module);
void moya_process_fake_function(SPTR func_sptr);
void moya_process_function(SPTR func_sptr,
                           const LL_ABI_Info& abi,
                           const LL_Function& function,
                           LL_Module& module);
void moya_process_common_block(SPTR sptr, const char* name);
void moya_process_fake_global(SPTR sptr, const char* gname, const char* fname);
void moya_process_closure(SPTR sptr, const char* tname, const char* fname);

void moya_func_begin(void);
void moya_func_end(void);
void moya_func_set_name(SPTR func_sptr, const char* name);

void moya_global_add_layout_descriptor(const char* name);

void moya_inst_add_final(int ilix, SPTR sptr, int64_t offset);
void moya_inst_add_gep(int ilix, int op1, int op2);
void moya_inst_add_ili(int ilix, int op1, int op2);
void moya_inst_add_offset(int ilix, int64_t offset, int prev);
void moya_inst_add_sdsc_offset(int ilix,
                               int acon,
                               int64_t offset,
                               int64_t base);
void moya_inst_add_str_offset(int curr, int actual);

void moya_type_add_typedesc(const char* base);

void moya_ilm_begin(int ilm, ILM_OP op);
void moya_ilm_end(int ilm);

template <typename... Args>
void moya_iprint(const char* fmt, Args... args) {
  if(moya_enabled() and moya_flang_verbose()) {
    moya_indent();
    fprintf(stderr, fmt, args...);
  }
}

template <typename... Args>
void moya_iprint(const char* msg) {
  if(moya_enabled() and moya_flang_verbose()) {
    moya_indent();
    fprintf(stderr, "%s", msg);
  }
}

#endif // MOYA_H_
