#include <sstream>
#include <string>
#include <vector>

#include <dlfcn.h>
#include <execinfo.h>

// Opaque handler to libMoya obtained from dlopen()
static void* moya_lib = nullptr;

static std::vector<std::string> parse_cmdline(const std::string& cmdline) {
  std::vector<std::string> toks;
  std::stringstream tok;
  bool quote = false;
  for(char c : cmdline.substr(1)) {
    if(std::isspace(c)) {
      if(quote) {
        tok << c;
      } else {
        std::string s = tok.str();
        if(s.length())
          toks.push_back(tok.str());
        tok.str("");
      }
    } else if(c == '"' or c == '\'') {
      tok << c;
      quote = not quote;
    } else {
      tok << c;
    }
  }

  // flg.cmdline starts and ends with "'". We ignore the first "'", so the last
  // character that we see will be a "'" that we ignore
  std::string s = tok.str();
  toks.push_back(s.substr(0, s.length() - 1));

  return toks;
}

static std::vector<std::string>
get_clang_plugins(const std::vector<std::string>& toks) {
  std::vector<std::string> libs;
  size_t i = 0;
  while(i < toks.size() - 4) {
    if((toks[i] == "-Xclang") and (toks[i + 1] == "-load")
       and (toks[i + 2] == "-Xclang")) {
      libs.push_back(toks[i + 3]);
      i += 3;
    }
    i += 1;
  }
  return libs;
}

extern "C" void* moya_fe_func(const char* fname) {
  return dlsym(moya_lib, fname);
}

bool moya_init_lib(const std::string& cmdline) {
  for(const std::string& lib : get_clang_plugins(parse_cmdline(cmdline)))
    if((moya_lib = dlopen(lib.c_str(), RTLD_LOCAL | RTLD_LAZY)))
      if(dlsym(moya_lib, "moya_is_frontend_fortran"))
        return true;
  return false;
}

std::string moya_get_json_file(const std::string& ilmfile) {
  return "/tmp/" + ilmfile.substr(0, ilmfile.length() - 3) + "json";
}

extern "C" bool moya_flang_verbose(void) {
  return getenv("MOYA_FLANG_VERBOSE");
}

extern "C" bool moya_enabled(void) {
  return moya_lib;
}

extern "C" void moya_print_backtrace(void) {
  if(not moya_enabled())
    return;
  
  void* array[20];
  size_t size;
  char** strings;
  size_t i;

  size = backtrace(array, 20);
  strings = backtrace_symbols(array, size);

  for(i = 0; i < size; i++)
    fprintf(stderr, "%s\n", strings[i]);

  free(strings);
}
