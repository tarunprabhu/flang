#ifndef FLANG_MOYAUTIL_H_
#define FLANG_MOYAUTIL_H_

// Declarations common to flang1 and flang2

#include <dlfcn.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void moya_print_backtrace(void);
bool moya_flang_verbose(void);
bool moya_enabled(void);
void moya_indent();

void* moya_fe_func(const char*);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

// Only for flang2

#ifdef __cplusplus

#include <string>
#include <cstdio>

#define DECL_API_FUNC_IMPL(name, suffix, type) \
  using name##suffix = type;                   \
  static name##suffix name = nullptr

#define DECL_API_FUNC(name, type) DECL_API_FUNC_IMPL(name, _t, type)

#define INIT_API_FUNC_IMPL(name, suffix)                        \
  do {                                                          \
    name = reinterpret_cast<name##suffix>(moya_fe_func(#name)); \
  } while(0)

#define INIT_API_FUNC(name) INIT_API_FUNC_IMPL(name, _t)

bool moya_init_lib(const std::string&);
std::string moya_get_json_file(const std::string&);

template<typename... Args> 
void moya_error(const char* fmt, Args... args) {
  if(moya_enabled()) {
    fprintf(stderr, fmt, args...);
    fprintf(stderr, "%d", *(int*)(0x1));
  }
}

template<typename... Args>
void moya_error(const char* fmt) {
  if(moya_enabled()) {
    fprintf(stderr, "%s", fmt);
    fprintf(stderr, "%d", *(int*)(0x1));
  }
}

template<typename... Args>
void moya_print(const char* fmt, Args... args) {
  if(moya_enabled() and moya_flang_verbose()) {
    fprintf(stderr, fmt, args...);
  }
}

template<typename... Args>
void moya_print(const char* msg) {
  if(moya_enabled() and moya_flang_verbose()) {
    fprintf(stderr, "%s", msg);
  }
}

DECL_API_FUNC(moya_module_initialize, void(*)(int, int, const char*));
DECL_API_FUNC(moya_module_serialize, void(*)(void));

#endif // __cplusplus

#endif // FLANG_MOYAUTIL_H_
