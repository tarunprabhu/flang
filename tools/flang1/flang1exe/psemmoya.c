/** \file
    \brief Semantic analyzer routines which process Moya statements.
 */

#include "gbldefs.h"
#include "global.h"
#include "gramsm.h"
#include "gramtk.h"
#include "symtab.h"
#include "semant.h"
#include "scan.h"
#include "semstk.h"
#include "ast.h"
#include "direct.h"

/**
   \param rednum   reduction number
   \param top      top of stack after reduction
 */
void
psemmoya(int rednum, SST *top)
{
  SST_ASTP(LHS, 0);
}
