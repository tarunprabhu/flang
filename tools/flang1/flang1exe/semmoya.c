/** \file
    \brief  semantic analyzer routines which process Moya statements.
 */

#include "gbldefs.h"
#include "global.h"
#include "gramsm.h"
#include "gramtk.h"
#include "error.h"
#include "symtab.h"
#include "symutl.h"
#include "dtypeutl.h"
#include "semant.h"
#include "scan.h"
#include "semstk.h"
#include "ast.h"
#include "direct.h"
#include "pragma.h"
#include "x86.h"
#include "llmputil.h"
#include "atomic_common.h"

#include "moya.h"

typedef uint64_t RegionID;

static int mk_moyafunc(char *rtn);
static void emit_begin_region(RegionID region);
static void emit_end_region(RegionID region);

static RegionID curr_region = 0x3;
static int prev_stmt = 0;

void semmoya(const char* file, unsigned line, unsigned column) {
  RegionID region;

  /* The moya region "end" directives are matched with the syntactically nearest 
     begin directive. This function will be called after every line (???: Or is
     it after every statement - there could be multiple statements on a line 
     separated by semicolons). When the first statement after a begin directive 
     is seen, moya_region_current() will return a non-zero value which is the 
     RegionID. When the first statment after an end directive is seen, 
     moya_region_current() will return 0 */
  region = moya_get_region_id(file, line, column);
  if(region > curr_region) {
    curr_region = region;
    emit_begin_region(curr_region);
  } else if(region < curr_region) {
    emit_end_region(curr_region);
    curr_region = region;
  }
  prev_stmt = (int)STD_PREV(0);
}

/* At this point, we're not going to bother supported any other kinds of regions
   other than a JIT region */
void emit_begin_region(RegionID region) {
  int ast;
  int id;

  id = mk_isz_cval(region, DT_INT8);
  ast = begin_call(A_CALL, mk_moyafunc("__moya_enter_region"), 1);
  add_arg(mk_unop(OP_VAL, id, A_DTYPEG(id)));

  add_stmt_after(ast, prev_stmt);
}

void emit_end_region(RegionID region) {
  int ast;
  int id;

  id = mk_isz_cval(region, DT_INT8);
  ast = begin_call(A_CALL, mk_moyafunc("__moya_exit_region"), 1);
  add_arg(mk_unop(OP_VAL, id, A_DTYPEG(id)));

  add_stmt_after(ast, prev_stmt);
}

static int
mk_moyafunc(char *rtlRtn)
{
  int sptr;
  sptr = sym_mkfunc(rtlRtn, DT_NONE);
  NODESCP(sptr, 1);
#ifdef SDSCSAFEG
  SDSCSAFEP(sptr, 1);
#endif
  INDEPP(sptr, 1);
  TYPDP(sptr, 1);     /* force external statement */
  INTERNALP(sptr, 0); /* these are not internal functions */
  INTENTP(sptr, INTENT_IN);
  return sptr;
}
