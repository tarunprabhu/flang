#ifndef FLANG1_MOYA_H_
#define FLANG1_MOYA_H_

#include "moyautil.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void moya_init(int freeform, const char* cmdline, const char* ilmfile);
void moya_fini();
void moya_parse_directive(const char* str,
                          const char* filename,
                          unsigned line,
                          unsigned col);
uint64_t moya_get_region_id(const char* filename, unsigned line, unsigned col);
int moya_add_allocated_type(int type);
void moya_add_local_array(const char* name);
void moya_add_interface_param(unsigned argno, int dtype);
void moya_add_tmp_array(const char* base);
void moya_add_global_category(const char* name, const char* module);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // FLANG1_MOYA_H_
