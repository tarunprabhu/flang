#include "moya.h"
#include "gbldefs.h"
#include "global.h"
#include "moyautil.h"
#include "symacc.h"

#include <dlfcn.h>

#include <cstdio>
#include <string>

DECL_API_FUNC(moya_module_parse_directive,
              void (*)(const char*, const char*, unsigned, unsigned));
DECL_API_FUNC(moya_module_get_region_id,
              uint64_t (*)(const char*, unsigned, unsigned));
DECL_API_FUNC(moya_module_add_allocated_type,
              int (*)(const char*, const char*, const char*, int));
DECL_API_FUNC(moya_module_add_local_array_1,
              void (*)(const char*, const char*, const char*, const char*));
DECL_API_FUNC(moya_module_add_interface_param,
              void(*)(const char*, const char*, const char*, unsigned, int));
DECL_API_FUNC(moya_module_add_global_category,
              void (*)(const char*, const char*));

extern "C" void
moya_init(int freeform, const char* cmdline, const char* ilmfile) {
  if(moya_init_lib(cmdline)) {
    INIT_API_FUNC(moya_module_initialize);
    INIT_API_FUNC(moya_module_parse_directive);
    INIT_API_FUNC(moya_module_get_region_id);
    INIT_API_FUNC(moya_module_add_allocated_type);
    INIT_API_FUNC(moya_module_add_local_array_1);
    INIT_API_FUNC(moya_module_add_interface_param);
    INIT_API_FUNC(moya_module_add_global_category);
    INIT_API_FUNC(moya_module_serialize);
  }

  // FIXME: This is not correct. Fortran 90+ could also be written in fixed
  // form.
  if(moya_enabled()) {
    std::string json = moya_get_json_file(ilmfile);
    moya_module_initialize(not freeform, true, json.c_str());
  }
}

extern "C" void moya_fini() {
  if(not moya_enabled())
    return;

  moya_module_serialize();
}

extern "C" void moya_parse_directive(const char* str,
                                     const char* filename,
                                     unsigned line,
                                     unsigned col) {
  if(not moya_enabled())
    return;

  std::string s(str);
  while(std::isspace(s.back()))
    s.pop_back();
  moya_module_parse_directive(s.c_str(), filename, line, col);
}

extern "C" uint64_t
moya_get_region_id(const char* filename, unsigned line, unsigned col) {
  if(not moya_enabled())
    return 0x3;

  return moya_module_get_region_id(filename, line, col);
}

extern "C" int moya_add_allocated_type(int type) {
  if(not moya_enabled())
    return 0;

  if(gbl.rutype == RU_PROG)
    return moya_module_add_allocated_type("MAIN", "", "", type);
  else
    return moya_module_add_allocated_type(SYMNAME(gbl.currsub),
                                          SYMNAME(gbl.outersub),
                                          SYMNAME(gbl.currmod),
                                          type);
}

extern "C" void moya_add_local_array(const char* local) {
  if(not moya_enabled())
    return;

  if(gbl.rutype == RU_PROG)
    moya_module_add_local_array_1("MAIN", "", "", local);
  else
    moya_module_add_local_array_1(SYMNAME(gbl.currsub),
                                  SYMNAME(gbl.outersub),
                                  SYMNAME(gbl.currmod),
                                  local);
}

extern "C" void moya_add_tmp_array(const char* base) {
  if(not moya_enabled())
    return;

  std::string local = base;
  local.append("$p");

  if(gbl.rutype == RU_PROG)
    moya_module_add_local_array_1("MAIN", "", "", local.c_str());
  else
    moya_module_add_local_array_1(SYMNAME(gbl.currsub),
                                  SYMNAME(gbl.outersub),
                                  SYMNAME(gbl.currmod),
                                  local.c_str());
}

extern "C" void moya_add_interface_param(unsigned argno, int dtype) {
  if(not moya_enabled())
    return;

  if(gbl.rutype == RU_PROG)
    moya_module_add_interface_param("MAIN", "", "", argno, dtype);
  else
    moya_module_add_interface_param(SYMNAME(gbl.currsub),
                                    SYMNAME(gbl.outersub),
                                    SYMNAME(gbl.currmod),
                                    argno,
                                    dtype);
}

extern "C" void moya_add_global_category(const char* name, const char* module) {
  if(not moya_enabled())
    return;

  moya_module_add_global_category(name, module);
}
